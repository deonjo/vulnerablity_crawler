"""
puts vulnerability info into the db
cve, summary, repo_location, commit_number

"""
import re
from lxml import etree
import urllib2
import psycopg2

# Connect to an existing database
conn = psycopg2.connect(dbname="patch_db", user="patch_user")
# Open a cursor to perform database operations
cur = conn.cursor()

def search_results(filename):
    print filename

    f = open(filename)
    tree = etree.parse(f)
    entry_nodes = tree.xpath('//prefix:entry', namespaces={'prefix':'http://scap.nist.gov/schema/feed/vulnerability/2.0'})
    for entry_node in entry_nodes:
        cve = entry_node.xpath('@id')[0]
        summary_node = entry_node.xpath('./prefix:summary', namespaces={'prefix':'http://scap.nist.gov/schema/vulnerability/0.4'})[0]
        summary = summary_node.text
        cwe_nodes = entry_node.xpath('./prefix:cwe', namespaces={'prefix':'http://scap.nist.gov/schema/vulnerability/0.4'})
        if len(cwe_nodes) > 0:
            cwe = cwe_nodes[0].xpath('@id')[0]
        else:
            cwe = '0'
        published_datetime_node = entry_node.xpath('./prefix:published-datetime', namespaces={'prefix':'http://scap.nist.gov/schema/vulnerability/0.4'})[0]
        published_datetime = published_datetime_node.text
        modified_datetime_node = entry_node.xpath('./prefix:last-modified-datetime', namespaces={'prefix':'http://scap.nist.gov/schema/vulnerability/0.4'})[0]
        modified_datetime = modified_datetime_node.text
        reference_group_nodes = entry_node.xpath('./prefix:references', namespaces={'prefix':'http://scap.nist.gov/schema/vulnerability/0.4'})
        for reference_group_node in reference_group_nodes:
            reference_nodes = reference_group_node.xpath('./prefix:reference', namespaces={'prefix':'http://scap.nist.gov/schema/vulnerability/0.4'})
            for reference_node in reference_nodes:
                url = reference_node.xpath('@href')[0]
                if len(url)>0 and re.search('.*/commit/.*[0-9A-Fa-f]{40}', url):
                    git_type_1(url, cve, cwe, summary, published_datetime, modified_datetime)
                elif len(url)>0 and re.search('.*git\..*[0-9A-Fa-f]{40}.*', url):
                    git_type_2(url, cve, cwe, summary, published_datetime, modified_datetime)

def git_type_1(url, cve, cwe, summary, published_datetime, modified_datetime):
    repo_location = re.split('commit|;', url)[0]
    commit_number = re.search('[0-9A-Fa-f]{40}', url).group(0)
    if len(repo_location) > 0 and len(commit_number) > 0:
        insert_revision(cve, cwe, summary, published_datetime, modified_datetime, repo_location, commit_number)

def git_type_2(url, cve, cwe, summary, published_datetime, modified_datetime):
    try:
        summary_url = re.split(';', url)[0]
        summary_page = urllib2.urlopen(summary_url)
        parser = etree.HTMLParser()
        git_tree = etree.parse(summary_page, parser)
        url_nodes = git_tree.xpath("//tr[@class='metadata_url']/td")
        repo_location = ''
        for url_node in url_nodes:
            if url_node.text and re.match("git://", url_node.text):
                repo_location = url_node.text

        if not(repo_location):
            location_nodes = git_tree.xpath("//div[@class='page_header']/a")
            if len(location_nodes) > 2:
                repo_location = location_nodes[1].text + '/' + location_nodes[2].text
        commit_number = re.search('[0-9A-Fa-f]{40}', url).group(0)
        if len(repo_location) > 0 and len(commit_number) > 0:
            insert_revision(cve, cwe, summary, published_datetime, modified_datetime, repo_location, commit_number)
    except urllib2.URLError:
        #print "bad git url"
        pass
    
    
def insert_revision(cve, cwe, summary, published_datetime, modified_datetime, repo_location, commit_number):
    print cve
    cur.execute("SELECT * FROM locations where location = %s;", (repo_location,))
    if cur.fetchone() == None:
        cur.execute("INSERT INTO locations (location) VALUES (%s)", (repo_location,))
        conn.commit()
    cur.execute("SELECT location_id FROM locations where location = %s;", (repo_location,))
    location_id = cur.fetchone()[0]
    cur.execute("SELECT * FROM entries where cve = %s;", (cve,))
    if cur.fetchone() == None:
        cur.execute("INSERT INTO entries (cve, cwe, summary, published_datetime, modified_datetime, commit_number) VALUES (%s, %s, %s, %s, %s, %s)", (cve, cwe, summary, published_datetime, modified_datetime, commit_number))
        conn.commit()
        cur.execute("INSERT INTO location_mapping (cve, location_id) VALUES (%s, %s)", (cve, location_id))
        conn.commit()
    

def main():
    filenames = ['nvdcve-2.0-2002.xml', 'nvdcve-2.0-2003.xml', 'nvdcve-2.0-2004.xml', 'nvdcve-2.0-2005.xml', 'nvdcve-2.0-2006.xml', 'nvdcve-2.0-2007.xml', 'nvdcve-2.0-2008.xml', 'nvdcve-2.0-2009.xml', 'nvdcve-2.0-2010.xml', 'nvdcve-2.0-2011.xml', 'nvdcve-2.0-2012.xml', 'nvdcve-2.0-modified.xml', 'nvdcve-2.0-recent.xml']
    for filename in filenames:
        search_results(filename)

if __name__ == "__main__":
  main()
