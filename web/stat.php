<?
/*
$conn = mysql_connect("SQL09.FREEMYSQL.NET","cs161proj","patchuser");
if (!$conn){
	die('could not connect : '.mysql_error());
}
mysql_select_db("cs161proj",$conn);
 */
$conn = mysql_connect(
	$server = getenv('MYSQL_DB_HOST'),
	$username = getenv('MYSQL_USERNAME'),
	$password = getenv('MYSQL_PASSWORD'));
if (!$conn){
	print 'hi'.$server.' '.$username.' '.$password;
	die('could not connect : '.mysql_error());
}
mysql_select_db(getenv('MYSQL_DB_NAME'));
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>CS161 Vulnerability Patches</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="google-code-prettify/prettify.css" type="text/css" rel="stylesheet">
    <script type="text/javascript" src="google-code-prettify/prettify.js"></script>

    <!-- Le styles -->
    <link href="./css/bootstrap.css" rel="stylesheet">
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
      .sidebar-nav {
        padding: 9px 0;
      }
    </style>
    <link href="./css/bootstrap-responsive.css" rel="stylesheet">

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="../assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
  </head>

  <body onload="prettyPrint()">

    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="./index.php">CS161 Vulnerability Patches</a>
          <!--<div class="btn-group pull-right">
            <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
              <i class="icon-user"></i> Terence
              <span class="caret"></span>
            </a>
            <ul class="dropdown-menu">
              <li><a href="#">Profile</a></li>
              <li class="divider"></li>
              <li><a href="#">Sign Out</a></li>
            </ul>
          </div>-->
	  <div class="nav-collapse">
            <ul class="nav">
              <li><a href="./index.php">Home</a></li>
              <li><a href="./cve.php">CVE</a></li>
              <li><a href="./examples.php">Examples</a></li>
              <li class="active"><a href="./stat.php">Statistics</a></li>
            </ul>
	    </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

    <div class="container-fluid">
      <div class="row-fluid">
        <div class="span3">
          <div class="well sidebar-nav">
            <ul class="nav nav-list">
              <li class="nav-header">Vulnerability Types</li>
<?
              $result = mysql_query("SELECT * FROM vuln_type");

	      while($row = mysql_fetch_assoc($result)){
              		echo '<li><a href="cve.php?vuln_type='.$row['CWE-ID'].'">'.$row['Name'].'</a></li>';
		}
?>
            </ul>
          </div><!--/.well -->
        </div><!--/span-->
        <div class="span9">
            <h2>Vulnerabilities Breakdown by Type</h2>

<script type="text/javascript" src="//ajax.googleapis.com/ajax/static/modules/gviz/1.0/chart.js"> {"dataSourceUrl":"//docs.google.com/spreadsheet/tq?key=0Ajyy8a9YMK-MdEsxRkU0VWgtNy1RWFdaajlHU0phTGc&transpose=0&headers=0&range=A1%3AB18&gid=0&pub=1","options":{"vAxes":[{"useFormatFromData":true,"viewWindowMode":"pretty","viewWindow":{}},{"useFormatFromData":true,"viewWindowMode":"pretty","viewWindow":{}}],"pieHole":0,"booleanRole":"certainty","colors":["#3366CC","#DC3912","#FF9900","#109618","#990099","#0099C6","#DD4477","#66AA00","#B82E2E","#316395","#994499","#22AA99","#AAAA11","#6633CC","#E67300","#8B0707","#651067","#329262","#5574A6","#3B3EAC","#B77322","#16D620","#B91383","#F4359E","#9C5935","#A9C413","#2A778D","#668D1C","#BEA413","#0C5922","#743411"],"is3D":true,"hAxis":{"useFormatFromData":true},"width":800,"height":400},"state":{},"view":{"columns":[{"calc":"stringify","type":"string","sourceColumn":0},1]},"chartType":"PieChart","chartName":"Chart 1"} </script>
	    <div class="well">
		This diagram shows buffer overflow is relatively common.
	    </div>

            <h2>Vulnerabilities Breakdown by File Types</h2>

<script type="text/javascript" src="//ajax.googleapis.com/ajax/static/modules/gviz/1.0/chart.js"> {"dataSourceUrl":"//docs.google.com/spreadsheet/tq?key=0Ajyy8a9YMK-MdEsxRkU0VWgtNy1RWFdaajlHU0phTGc&transpose=0&headers=0&range=G1%3AH38&gid=0&pub=1","options":{"vAxes":[{"title":null,"useFormatFromData":true,"minValue":null,"viewWindowMode":"pretty","viewWindow":{"min":null,"max":null},"maxValue":null},{"useFormatFromData":true,"viewWindowMode":"pretty","viewWindow":{}}],"pieHole":0,"useFormatFromData":true,"booleanRole":"certainty","title":"","colors":["#3366CC","#DC3912","#FF9900","#109618","#990099","#0099C6","#DD4477","#66AA00","#B82E2E","#316395","#994499","#22AA99","#AAAA11","#6633CC","#E67300","#8B0707","#651067","#329262","#5574A6","#3B3EAC","#B77322","#16D620","#B91383","#F4359E","#9C5935","#A9C413","#2A778D","#668D1C","#BEA413","#0C5922","#743411"],"is3D":true,"hAxis":{"useFormatFromData":true,"viewWindowMode":"pretty","viewWindow":{}},"width":800,"height":400},"state":{},"chartType":"PieChart","chartName":"Chart 1"} </script>

<h2>Vulnerabilities Breakdown by Year</h2>

<script type="text/javascript" src="//ajax.googleapis.com/ajax/static/modules/gviz/1.0/chart.js"> {"dataSourceUrl":"//docs.google.com/spreadsheet/tq?key=0Ajyy8a9YMK-MdEsxRkU0VWgtNy1RWFdaajlHU0phTGc&transpose=0&headers=0&range=D1%3AE8&gid=0&pub=1","options":{"vAxes":{"0":{"useFormatFromData":true},"1":{"useFormatFromData":true}},"booleanRole":"certainty","useFormatFromData":true,"height":534,"animation":{"duration":500},"legend":"none","width":800,"useFirstColumnAsDomain":true,"hAxis":{"useFormatFromData":true,"viewWindowMode":"pretty","viewWindow":{}},"isStacked":false},"state":{},"view":{"columns":[{"calc":"stringify","type":"string","sourceColumn":0},1]},"chartType":"BarChart","chartName":"Chart 3"} </script>
        </div><!--/span-->
      </div><!--/row-->

      <hr>

      <footer>
        <p>Terence Tam, Cheng-Yu Hong, Peter Cheng, Gideon Chia, Patrick Bernal, Jonathan Zernik</p>
      </footer>

    </div><!--/.fluid-container-->

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="/js/jquery.js"></script>
    <script src="/js/bootstrap-transition.js"></script>
    <script src="/js/bootstrap-alert.js"></script>
    <script src="/js/bootstrap-modal.js"></script>
    <script src="/js/bootstrap-dropdown.js"></script>
    <script src="/js/bootstrap-scrollspy.js"></script>
    <script src="/js/bootstrap-tab.js"></script>
    <script src="/js/bootstrap-tooltip.js"></script>
    <script src="/js/bootstrap-popover.js"></script>
    <script src="/js/bootstrap-button.js"></script>
    <script src="/js/bootstrap-collapse.js"></script>
    <script src="/js/bootstrap-carousel.js"></script>
    <script src="/js/bootstrap-typeahead.js"></script>

  </body>
</html>

