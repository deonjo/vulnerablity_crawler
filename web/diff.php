<?
/*
$conn = mysql_connect("SQL09.FREEMYSQL.NET","cs161proj","patchuser");
if (!$conn){
	die('could not connect : '.mysql_error());
}
mysql_select_db("cs161proj",$conn);
 */
$conn = mysql_connect(
	$server = getenv('MYSQL_DB_HOST'),
	$username = getenv('MYSQL_USERNAME'),
	$password = getenv('MYSQL_PASSWORD'));
if (!$conn){
	print 'hi'.$server.' '.$username.' '.$password;
	die('could not connect : '.mysql_error());
}
mysql_select_db(getenv('MYSQL_DB_NAME'));
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>CS161 Vulnerability Patches</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="./css/bootstrap.css" rel="stylesheet">
    <link href="google-code-prettify/prettify.css" type="text/css" rel="stylesheet">
    <script type="text/javascript" src="google-code-prettify/prettify.js"></script>
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
      .sidebar-nav {
        padding: 9px 0;
      }
    </style>
    <link href="./css/bootstrap-responsive.css" rel="stylesheet">

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="../assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
  </head>

  <body onload="prettyPrint()">

    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="#">CS161 Vulnerability Patches</a>
          <!--<div class="btn-group pull-right">
            <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
              <i class="icon-user"></i> Terence
              <span class="caret"></span>
            </a>
            <ul class="dropdown-menu">
              <li><a href="#">Profile</a></li>
              <li class="divider"></li>
              <li><a href="#">Sign Out</a></li>
            </ul>
          </div>-->
	  <div class="nav-collapse">
            <ul class="nav">
              <li><a href="./index.php">Home</a></li>
              <li class="active"><a href="./cve.php">CVE</a></li>
              <li><a href="./examples.php">Examples</a></li>
              <li><a href="./stat.php">Statistics</a></li>
            </ul>
	    </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

    <div class="container-fluid">
      <div class="row-fluid">
        <div class="span3">
          <div class="well sidebar-nav">
            <ul class="nav nav-list">
              <li class="nav-header">Vulnerabilitiy Types</li>
<?
              $result = mysql_query("SELECT * FROM vuln_type");

	      while($row = mysql_fetch_assoc($result)){
              		echo '<li><a href="cve.php?vuln_type='.$row['CWE-ID'].'">'.$row['Name'].'</a></li>';
		}
?>
            </ul>
          </div><!--/.well -->
        </div><!--/span-->
        <div class="span9">
          <div class="hero-unit">
	<?
		$three = "";
		$two = "";
		$one = "";
		$cve = $_GET['cve'];
		if ($_GET['mode']=="func"){
			$mode = "function";
			$three = "active";
		}
		else if ($_GET['mode']=="diff"){
			$mode = "diff";
			$two = "active";
		}
		else{
			$mode = "prediff";
			$one = "active";
		}
              $result = mysql_query("SELECT * FROM location_mappings INNER JOIN location ON location.location_id = location_mappings.location_id WHERE location_mappings.cve='".$cve."'");

	      if($row = mysql_fetch_assoc($result)){
              		echo '<h2>Repo: '.$row['location'].'</h2>';
		}
	      else{
       			echo "<h2>Sample Diff Patch</h2>";
	      }
	      $result = mysql_query("SELECT summary FROM patch_db WHERE cve='".$cve."'");
	      if($row = mysql_fetch_assoc($result)){
              		echo '<h5>'.$row['summary'].'</h2>';
	      }
	echo '<div class="btn-group">';
	echo '<a class="btn '.$one.'" href="diff.php?cve='.$cve.'&mode=prediff">Prepatched</a>';
	echo '<a class="btn '.$two.'" href="diff.php?cve='.$cve.'&mode=diff">Context Diff</a>';
	echo '<a class="btn '.$three.'" href="diff.php?cve='.$cve.'&mode=func">Function Diff</a>';
	echo '</div>';
	//echo '<p><a class="btn btn-primary" href="diff.php?cve='.$cve.'&mode=diff">Diff</a>';
	//echo '<a class="btn btn-primary" href="diff.php?cve='.$cve.'&mode=func">Func</a>';
	?>
       <pre class = "prettyprint linenums">
	<? echo file_get_contents('./diff/'.$cve.'/'.$mode.'.patch'); ?>
       </pre>
          </div>
          
        </div><!--/span-->
      </div><!--/row-->

      <hr>

      <footer>
        <p>Terence Tam, Cheng-Yu Hong, Peter Cheng, Gideon Chia, Patrick Bernal, Jonathan Zernik</p>
      </footer>

    </div><!--/.fluid-container-->

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="/js/jquery.js"></script>
    <script src="/js/bootstrap-transition.js"></script>
    <script src="/js/bootstrap-alert.js"></script>
    <script src="/js/bootstrap-modal.js"></script>
    <script src="/js/bootstrap-dropdown.js"></script>
    <script src="/js/bootstrap-scrollspy.js"></script>
    <script src="/js/bootstrap-tab.js"></script>
    <script src="/js/bootstrap-tooltip.js"></script>
    <script src="/js/bootstrap-popover.js"></script>
    <script src="/js/bootstrap-button.js"></script>
    <script src="/js/bootstrap-collapse.js"></script>
    <script src="/js/bootstrap-carousel.js"></script>
    <script src="/js/bootstrap-typeahead.js"></script>

  </body>
</html>

