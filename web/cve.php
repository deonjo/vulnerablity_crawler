<?
/*
$conn = mysql_connect("SQL09.FREEMYSQL.NET","cs161proj","patchuser");
if (!$conn){
	die('could not connect : '.mysql_error());
}
mysql_select_db("cs161proj",$conn);
 */
$conn = mysql_connect(
	$server = getenv('MYSQL_DB_HOST'),
	$username = getenv('MYSQL_USERNAME'),
	$password = getenv('MYSQL_PASSWORD'));
if (!$conn){
	print 'hi'.$server.' '.$username.' '.$password;
	die('could not connect : '.mysql_error());
}
mysql_select_db(getenv('MYSQL_DB_NAME'));
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>CS161 Vulnerability Patches</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="./css/bootstrap.css" rel="stylesheet">
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
      .sidebar-nav {
        padding: 9px 0;
      }
    </style>
    <link href="./css/bootstrap-responsive.css" rel="stylesheet">

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="../assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
  </head>

  <body>

    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="/index.php">CS161 Vulnerability Patches</a>
          <!--<div class="btn-group pull-right">
            <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
              <i class="icon-user"></i> Terence
              <span class="caret"></span>
            </a>
            <ul class="dropdown-menu">
              <li><a href="#">Profile</a></li>
              <li class="divider"></li>
              <li><a href="#">Sign Out</a></li>
            </ul>
          </div>-->
	  <div class="nav-collapse">
            <ul class="nav">
              <li><a href="./index.php">Home</a></li>
              <li class="active"><a href="./cve.php">CVE</a></li>
              <li><a href="./examples.php">Examples</a></li>
              <li><a href="./stat.php">Statistics</a></li>
            </ul>
	    </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

    <div class="container-fluid">
      <div class="row-fluid">
        <div class="span3">
          <div class="well sidebar-nav">
            <ul class="nav nav-list">
              <li class="nav-header">Vulnerability Types</li>
<?
              $result = mysql_query("SELECT * FROM vuln_type");

	      while($row = mysql_fetch_assoc($result)){
              		echo '<li><a href="?vuln_type='.$row['CWE-ID'].'">'.$row['Name'].'</a></li>';
		}
?>
            </ul>
          </div><!--/.well -->
        </div><!--/span-->
        <div class="span9">
          <div class="hero-unit">
<?
	      if (!isset($_GET['vuln_type'])){
	      		echo 'please choose a vulnerability';
	      }
	      else{
	      $type = trim($_GET['vuln_type']);
	      $result2 = mysql_query("SELECT * FROM vuln_type WHERE `CWE-ID`='".$type."'");

	      while($row2 = mysql_fetch_assoc($result2)){
		      	echo '<h2>'.$row2['Name'].'</h2>';
		      	echo '<h6>'.$row2['Description'].'</h6>';
		}
	      $result = mysql_query("SELECT * FROM patch_db WHERE cwe='".$type."'");

	      echo '<table class="table table-bordered table-striped" width="100%">';
		        echo '<thead><tr>';
		      	echo '<th>Cve</th>';
		      	echo '<th>Summary</th>';
		      	echo '<th>Code</th>';
		      	echo '<th colspan="2">Diff Files</th>';
		        echo '</tr></thead><tbody>';
	      while($row = mysql_fetch_assoc($result)){
		        echo '<tr>';
		      	echo '<td>'.$row['cve'].'</td>';
		      	echo '<td>'.$row['summary'].'</td>';
			if(file_exists('./diff/'.$row['cve'].'/diff.patch')){

				echo '<td><a href="diff.php?cve='.$row['cve'].'&mode=prediff">pre</a></td>';
				echo '<td><a href="diff.php?cve='.$row['cve'].'&mode=diff">diff</a></td>';
				echo '<td><a href="diff.php?cve='.$row['cve'].'&mode=func">func</a></td>';
			}
			else{

		      	echo '<td>N/A</td>';
		      	echo '<td>N/A</td>';
		      	echo '<td>N/A</td>';
			}
			echo '</tr>';
		}
	      echo '</tbody></table>';
}
?>
          </div>
          
        </div><!--/span-->
      </div><!--/row-->

      <hr>

      <footer>
        <p>Terence Tam, Cheng-Yu Hong, Peter Cheng, Gideon Chia, Patrick Bernal, Jonathan Zernik</p>
      </footer>

    </div><!--/.fluid-container-->

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="/js/jquery.js"></script>
    <script src="/js/bootstrap-transition.js"></script>
    <script src="/js/bootstrap-alert.js"></script>
    <script src="/js/bootstrap-modal.js"></script>
    <script src="/js/bootstrap-dropdown.js"></script>
    <script src="/js/bootstrap-scrollspy.js"></script>
    <script src="/js/bootstrap-tab.js"></script>
    <script src="/js/bootstrap-tooltip.js"></script>
    <script src="/js/bootstrap-popover.js"></script>
    <script src="/js/bootstrap-button.js"></script>
    <script src="/js/bootstrap-collapse.js"></script>
    <script src="/js/bootstrap-carousel.js"></script>
    <script src="/js/bootstrap-typeahead.js"></script>

  </body>
</html>

