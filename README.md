#CS161 Vulnerability Project

## Vulnerability How-to

*1. download repo

	python git_downloader.py 

*2. search for repo with ./configure

	find ./ -name "configure"

*3. in psql, search for commit number

	SELECT commit_number FROM entries WHERE cve = 'CVE-20xx-xxxx'

*4. checkout the version before the fix

	git checkout <commit_number>
	git checkout HEAD^

*5. inside the repo source, compile

	./configure cc="gcc -save-temps" && make

*6. find the i file and do analysis

	add //@slice pragma stmt; inside the i file
	frama-c -impact-pragma <function_name> <your_file.i> -main <function_name> -impact-print
	(might need to add -cpp-command="gcc -E" and -lib-entry if necessary)
