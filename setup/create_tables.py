import psycopg2

# Connect to an existing database
conn = psycopg2.connect(dbname="patch_db", user="patch_user")

# Open a cursor to perform database operations
cur = conn.cursor()

# Execute a command: this creates a new table
cur.execute("CREATE TABLE entries (cve varchar(20) PRIMARY KEY, cwe varchar(20), summary text, published_datetime timestamp, modified_datetime timestamp, commit_number varchar(40));")
cur.execute("CREATE TABLE locations (location_id serial PRIMARY KEY, location varchar(100) UNIQUE NOT NULL);")
cur.execute("CREATE TABLE location_mapping (cve varchar(20) references entries(cve), location_id integer references locations(location_id));")

# Make the changes to the database persistent
conn.commit()

# Close communication with the database
cur.close()
conn.close()
