#!/bin/sh
cp pg_hba.conf /etc/postgresql/9.1/main/
/etc/init.d/postgresql reload

su postgres -c "dropdb patch_db"
su postgres -c "dropuser patch_user"
su postgres -c "createdb patch_db"
su postgres -c "createuser -s -w patch_user"
python create_tables.py